package com.azhiltsov.turtleplugin.psi;

import com.azhiltsov.turtleplugin.resources.TurtleLanguage;
import com.intellij.psi.tree.IElementType;
import org.jetbrains.annotations.NotNull;


public class TurtleElementType extends IElementType {
    public TurtleElementType(@NotNull final String debugName) {
        super(debugName, TurtleLanguage.INSTANCE);
    }
}
