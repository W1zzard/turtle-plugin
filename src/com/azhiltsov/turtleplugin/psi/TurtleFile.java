package com.azhiltsov.turtleplugin.psi;

import com.azhiltsov.turtleplugin.resources.TurtleFileType;
import com.azhiltsov.turtleplugin.resources.TurtleLanguage;
import com.intellij.extapi.psi.PsiFileBase;
import com.intellij.lang.Language;
import com.intellij.openapi.fileTypes.FileType;
import com.intellij.psi.FileViewProvider;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;

public class TurtleFile extends PsiFileBase {
    public TurtleFile(@NotNull FileViewProvider viewProvider) {
        super(viewProvider, TurtleLanguage.INSTANCE);
    }

    @NotNull
    @Override
    public FileType getFileType() {
        return TurtleFileType.INSTANCE;
    }

    @Override
    public String toString() {
        return "Turtel File";
    }

    @Nullable
    @Override
    public Icon getIcon(int flags) {
        return super.getIcon(flags);
    }
}
