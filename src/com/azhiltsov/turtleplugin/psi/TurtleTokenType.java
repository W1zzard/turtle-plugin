package com.azhiltsov.turtleplugin.psi;

import com.azhiltsov.turtleplugin.resources.TurtleLanguage;

import com.intellij.psi.tree.IElementType;
import org.jetbrains.annotations.NotNull;

public class TurtleTokenType extends IElementType {
    public TurtleTokenType(@NotNull final String debugName) {
        super(debugName, TurtleLanguage.INSTANCE);
    }

    @Override
    public String toString() {
        return "TurtleTokenType" + super.toString();
    }
}
