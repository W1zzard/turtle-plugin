package com.azhiltsov.turtleplugin;

import com.intellij.lexer.FlexAdapter;

import java.io.Reader;

public class TurtleLexerAdapter extends FlexAdapter {
    public TurtleLexerAdapter() {
        super(new TurtleLexer((Reader) null));
    }
}
