package com.azhiltsov.turtleplugin.resources;

import com.intellij.openapi.fileTypes.LanguageFileType;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;

public class TurtleFileType extends LanguageFileType {
    public static final TurtleFileType INSTANCE = new TurtleFileType();

    private TurtleFileType() {
        super(TurtleLanguage.INSTANCE);
    }

    @NotNull
    @Override
    public String getName() {
        return "Turtle file";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Turtle language file";
    }

    @NotNull
    @Override
    public String getDefaultExtension() {
        return "ttl";
    }

    @Nullable
    @Override
    public Icon getIcon() {
        return TurtleIcon.FILE;
    }
}
