package com.azhiltsov.turtleplugin.resources;

import com.intellij.lang.Language;

/**
 * Description of language.
 */
public class TurtleLanguage extends Language{
    public static final TurtleLanguage INSTANCE = new TurtleLanguage();

    private TurtleLanguage() {
        super("Turtle");
    }
}
