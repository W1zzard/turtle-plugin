package com.azhiltsov.turtleplugin.resources;

import com.intellij.openapi.util.IconLoader;

import javax.swing.*;

public class TurtleIcon {
    public static final Icon FILE = IconLoader.getIcon("/com/azhiltsov/turtleplugin/resources/icon/w3c_home.png");
}
