package com.azhiltsov.turtleplugin;

import com.intellij.lexer.FlexLexer;
import com.intellij.psi.tree.IElementType;
import com.azhiltsov.turtleplugin.psi.TurtleTypes;
import com.intellij.psi.TokenType;

%%

%class TurtleLexer
%implements FlexLexer
%unicode
%function advance
%type IElementType
%eof{  return;
%eof}


EOL=\R
WHITE_SPACE=\s+


%%
<YYINITIAL> {
  {WHITE_SPACE}      { return WHITE_SPACE; }

  "a-zA-Z"           { return A-ZA-Z; }
  "eE"               { return EE; }
  "A-Z"              { return A-Z; }
  "x00C0-x00D6"      { return X00C0-X00D6; }
  "A-F"              { return A-F; }


}

[^]                                                         { return TokenType.BAD_CHARACTER; }